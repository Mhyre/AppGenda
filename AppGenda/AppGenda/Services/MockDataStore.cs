﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using AppGenda.Models;
using Metier;

[assembly: Xamarin.Forms.Dependency(typeof(AppGenda.Services.MockDataStore))]
namespace AppGenda.Services
{
    public class MockDataStore : IDataStore<Jour>
    {
        List<Jour> jours;

        public MockDataStore()
        {
            jours = new List<Jour>();
            Agenda agenda = new Agenda("agendapp", jours);

            var mockJours = new List<Jour>
            {
                new Jour { Id= Guid.NewGuid().ToString(), liste_tache = new List<Tache>() },
                new Jour { Id= Guid.NewGuid().ToString(), liste_tache = new List<Tache>() },
                new Jour { Id= Guid.NewGuid().ToString(), liste_tache = new List<Tache>() },
                new Jour { Id= Guid.NewGuid().ToString(), liste_tache = new List<Tache>() },
                new Jour { Id= Guid.NewGuid().ToString(), liste_tache = new List<Tache>() },
                new Jour { Id= Guid.NewGuid().ToString(), liste_tache = new List<Tache>() },
           };

            foreach (var jour in mockJours)
            {
                jours.Add(jour);
            }
        }

        public async Task<bool> AddJourAsync(Jour jour)
        {
            jours.Add(jour);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateJourAsync(Jour jour)
        {
            var _jour = jours.Where((Jour arg) => arg.Id == jour.Id).FirstOrDefault();
            jours.Remove(_jour);
            jours.Add(jour);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteJourAsync(string id)
        {
            var _jour = jours.Where((Jour arg) => arg.Id == id).FirstOrDefault();
            jours.Remove(_jour);

            return await Task.FromResult(true);
        }

        public async Task<Jour> GetJourAsync(string id)
        {
            return await Task.FromResult(jours.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Jour>> GetJoursAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(jours);
        }

        public Task<bool> AddItemAsync(Jour item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> UpdateItemAsync(Jour item)
        {
            throw new NotImplementedException();
        }

        public Task<bool> DeleteItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<Jour> GetItemAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<Jour>> GetItemsAsync(bool forceRefresh = false)
        {
            throw new NotImplementedException();
        }
    }
}