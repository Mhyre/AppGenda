﻿using System;
using System.Collections.Generic;
using System.Text;
using MyMVVMToolkit;
using Metier;

namespace AppGenda.VM
{
    class TacheVM : BaseViewModel<Tache>
    {
        public TacheVM(Tache model)
        {
            Model = model;
        }

        public DateTime Date
        {
            get { return Model.Date; }
            set { SetProperty(Model.Date, value, () => Model.Date = value); }
        }
        public TimeSpan HourBegin
        {
            get { return Model.HourBegin; }
            set { SetProperty(Model.HourBegin, value, () => Model.HourBegin = value); }
        }
        public TimeSpan HourEnd
        {
            get { return Model.HourEnd; }
            set { SetProperty(Model.HourEnd, value, () => Model.HourEnd = value); }
        }
        public String Title
        {
            get { return Model.Title; }
            set { SetProperty(Model.Title, value, () => Model.Title = value); }
        }
        public String Description
        {
            get { return Model.Description; }
            set { SetProperty(Model.Description, value, () => Model.Description = value); }
        }
        public String Color
        {
            get { return Model.Color; }
            set { SetProperty(Model.Color, value, () => Model.Color = value); }
        }

    }
}
