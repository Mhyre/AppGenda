﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MyMVVMToolkit;
using AppGenda.Models;
using Metier;

namespace AppGenda.VM
{
    class AgendaVM : BaseViewModel<Agenda>
    {
        public AgendaVM(Agenda model)
        {
            Model = model;
            foreach (var item in Model.Liste_jour)
            {
                lesJoursVM.Add(new JourVM(item));
            }
        }

        public ObservableCollection<JourVM> LesJoursVM { get { return lesJoursVM; } }
        private ObservableCollection<JourVM> lesJoursVM = new ObservableCollection<JourVM>();
    }
}
