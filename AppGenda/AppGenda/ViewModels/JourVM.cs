﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using MyMVVMToolkit;
using AppGenda.Models;
using Metier;

namespace AppGenda.VM
{
    class JourVM : BaseViewModel<Jour>
    {
        public JourVM(Jour model)
        {
            Model = model;
            foreach (var item in Model.liste_tache)
            {
                lesTachesVM.Add(new TacheVM(item));
            }
        }

        public ObservableCollection<TacheVM> LesTachesVM { get { return lesTachesVM; } }
        private ObservableCollection<TacheVM> lesTachesVM = new ObservableCollection<TacheVM>();
    }
}
