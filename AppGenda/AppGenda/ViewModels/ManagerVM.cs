﻿using System;
using System.Collections.Generic;
using System.Text;
using MyMVVMToolkit;
using Metier;

namespace AppGenda.VM
{
    class ManagerVM : BaseViewModel<Manager>
    {
        ManagerVM(IDataManager dataManager)
        {
            Model = new Manager(dataManager);
        }
    }
}
