﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metier
{
    public class Manager
    {
        IDataManager DataManager { get; set; }
        List<Jour> e = new List<Jour>();
        Agenda Agenda { get; set; } 

        public Manager(IDataManager dataManager)
        {
            Agenda = new Agenda("L'Agenda", e);
            DataManager = dataManager;
            foreach (var n in DataManager.Taches)
            {
            }
        }

        void Add(Tache tache)
        {
            DataManager.Add(tache);
        }

        void Remove(Tache tache)
        {
            DataManager.Remove(tache);
        }

        void Update(Tache tache)
        {
            DataManager.Update(tache);
        }
    }
}
