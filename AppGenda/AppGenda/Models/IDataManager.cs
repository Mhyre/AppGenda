﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metier
{
    public interface IDataManager
    {
        IEnumerable<Tache> Taches { get; }

        void Add(Tache tache);
        void Remove(Tache tache);
        void Update(Tache tache);
    }
}
