﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Metier
{
    public class Tache
    {

        public DateTime Date { get; set; }
        public TimeSpan HourBegin { get; set; }
        public TimeSpan HourEnd { get; set; }
        public String Title { get; set; }
        public String Description { get; set; }
        public String Color { get; set; }
        /*
        public String location { get; set; }
        public bool alert { get; set; }
        public bool repeat { get; set; }
        */

        public Tache(String title, String description)
        {
            this.Title = title;
            this.Description = description;
        }


    }
}
