﻿using System;
using System.Collections.Generic;

namespace Metier
{
    public class Agenda
    {
        public string Nom { get; set; }

        public IEnumerable<Jour> Liste_jour { get; set; }

        public Agenda(string nom, List<Jour> jours)
        {
            this.Nom = nom;
            this.Liste_jour = jours;
        }
    }
}
